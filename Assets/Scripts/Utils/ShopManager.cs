﻿using UnityEngine;
using System.Collections;

// Unity Ads
using UnityEngine.Advertisements;

// Soomla store
#if UNITY_ANDROID || UNITY_IOS
using Soomla;
using Soomla.Store;
#endif

public class ShopManager : MonoBehaviour 
{
    public bool clicking;

    public static ShopManager instance = null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }
    }

    public void BuyButtonClick(int product)
    {
        // avoid multiple click
        if (clicking == true) return;

        clicking = true;

        StartCoroutine(ResetButtonClick());

#if UNITY_EDITOR
		int movesCount = 0;

        switch (product)
        {
        case 1:
			movesCount = 2;
            break;
        case 2:
			movesCount = 5;
			GameData.instance.SaveBeginFiveMoves(GameData.instance.GetBeginFiveMoves() + 1);
            break;
        case 3:
			movesCount = 10;
			GameData.instance.SaveBeginFiveMoves(GameData.instance.GetBeginFiveMoves() + 2);
            break;
		case 4:
			GameData.instance.SavePlayerCoin(GameData.instance.GetPlayerCoin() + Configure.instance.product1Coin);
			break;
		case 5:
			GameData.instance.SavePlayerCoin(GameData.instance.GetPlayerCoin() + Configure.instance.product2Coin);
			break;
        case 6:
			GameData.instance.SaveSingleBreaker(GameData.instance.GetSingleBreaker() + 1);
			break;
        case 7:
			GameData.instance.SaveRowBreaker(GameData.instance.GetRowBreaker() + 1);
			break;
		case 8:
			GameData.instance.SaveColumnBreaker(GameData.instance.GetColumnBreaker() + 1);
			break;
		case 9:
			GameData.instance.SaveOvenBreaker(GameData.instance.GetOvenBreaker() + 1);
			break;
		case 10:
			GameData.instance.SaveRainbowBreaker(GameData.instance.GetRainbowBreaker() + 1);
			break;
		case 11:
			GameData.instance.SaveBeginRainbow(GameData.instance.GetBeginRainbow() + 1);
			break;
		case 12:
			GameData.instance.SaveBeginBombBreaker(GameData.instance.GetBeginBombBreaker() + 1);
			break;
		case 13:
			GameData.instance.SavePlayerCoin(GameData.instance.GetPlayerCoin() + Configure.instance.product3Coin);
			break;
		case 14:
			GameData.instance.SavePlayerCoin(GameData.instance.GetPlayerCoin() + Configure.instance.product4Coin);
			break;
		case 15:
			GameData.instance.SavePlayerCoin(GameData.instance.GetPlayerCoin() + Configure.instance.product5Coin);
			break;
		case 16:
			GameData.instance.SavePlayerCoin(GameData.instance.GetPlayerCoin() + Configure.instance.product6Coin);
			break;
		case 17:
			GameData.instance.SavePlayerCoin(GameData.instance.GetPlayerCoin() + Configure.instance.product7Coin);
			break;
		case 18:
			GameData.instance.SavePlayerCoin(GameData.instance.GetPlayerCoin() + Configure.instance.product8Coin);
			break;			
        }

        // play sound
        AudioManager.instance.CoinAddAudio();

        // update text label
        UpdateCoinAmountLabel();

		if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "Play") {
			Booster.instance.singleAmount.text = GameData.instance.GetSingleBreaker().ToString();
			Booster.instance.rowAmount.text = GameData.instance.GetRowBreaker().ToString();
			Booster.instance.columnAmount.text = GameData.instance.GetColumnBreaker().ToString();
			Booster.instance.rainbowAmount.text = GameData.instance.GetRainbowBreaker().ToString();
			Booster.instance.ovenAmount.text = GameData.instance.GetOvenBreaker().ToString();
			Board.Instance.moveLeft += movesCount;
			Board.Instance.UITop.IncreaseMoves(movesCount);
			Board.Instance.UpdateCoinAmountLabel();
		}

#elif UNITY_ANDROID || UNITY_IOS
		StoreInventory.BuyItem("item_" + product);        
#endif

    }

    public void UpdateCoinAmountLabel()
    {
        if (gameObject.GetComponent<UIShopPopupPlay>())
        {
            gameObject.GetComponent<UIShopPopupPlay>().UpdateCoinAmountLabel();

            // also update on lose popup
            var losePopup = GameObject.Find("LosePopup(Clone)");

            if (losePopup)
            {
                losePopup.GetComponent<UILosePopup>().coinText.text = GameData.instance.GetPlayerCoin().ToString();
            }
        }
        else if (GameObject.Find("MapScene"))
        {
            GameObject.Find("MapScene").GetComponent<MapScene>().UpdateCoinAmountLabel();
        }
    }

    IEnumerator ResetButtonClick()
    {
        yield return new WaitForSeconds(1f);

        clicking = false;
    }

    public void WatchVideoForCoinButtonClick()
    {
        // avoid multiple click
        if (clicking == true) return;

        clicking = true;

        StartCoroutine(ResetButtonClick());

        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };

            Advertisement.Show("rewardedVideo", options);
        }
        else
        {
            //Debug.Log("Unity Ads: Rewarded video is not ready.");
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                //Debug.Log("Unity Ads: The ad was successfully shown.");

                // plus coin
                GameData.instance.SavePlayerCoin(GameData.instance.GetPlayerCoin() + Configure.instance.watchVideoCoin);

                // play add coin sound
                AudioManager.instance.CoinAddAudio();

                // update text label
                UpdateCoinAmountLabel();

                break;
            case ShowResult.Skipped:
                //Debug.Log("Unity Ads: The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                //Debug.LogError("Unity Ads: The ad failed to be shown.");
                break;
        }
    }
}
