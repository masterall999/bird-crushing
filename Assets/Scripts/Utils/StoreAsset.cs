﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Soomla.Store;

public class StoreAsset : IStoreAssets
{
    public int GetVersion()
    {
        return 3;
    }

    public VirtualCurrency[] GetCurrencies()
    {
        return new VirtualCurrency[] { };
    }

    public VirtualGood[] GetGoods()
    {
		return new VirtualGood[] { NO_ADS_LTVG, PRODUCT_1, PRODUCT_2, PRODUCT_3, PRODUCT_4, PRODUCT_5, PRODUCT_6, PRODUCT_7, PRODUCT_8, PRODUCT_9, PRODUCT_10, PRODUCT_11, PRODUCT_12, PRODUCT_13, PRODUCT_14, PRODUCT_15, PRODUCT_16, PRODUCT_17, PRODUCT_18};
    }

    public VirtualCurrencyPack[] GetCurrencyPacks()
    {
        return new VirtualCurrencyPack[] { };
    }

    public VirtualCategory[] GetCategories()
    {
        return new VirtualCategory[] { };
    }

    public const string NO_ADS_ID = "no_ads";
    public const string PRODUCT_1_ID = "item_1";
    public const string PRODUCT_2_ID = "item_2";
    public const string PRODUCT_3_ID = "item_3";
    public const string PRODUCT_4_ID = "item_4";
    public const string PRODUCT_5_ID = "item_5";
	public const string PRODUCT_6_ID = "item_6";
	public const string PRODUCT_7_ID = "item_7";
	public const string PRODUCT_8_ID = "item_8";
	public const string PRODUCT_9_ID = "item_9";
	public const string PRODUCT_10_ID = "item_10";
	public const string PRODUCT_11_ID = "item_11";
	public const string PRODUCT_12_ID = "item_12";
	public const string PRODUCT_13_ID = "item_13";
	public const string PRODUCT_14_ID = "item_14";
	public const string PRODUCT_15_ID = "item_15";
	public const string PRODUCT_16_ID = "item_16";
	public const string PRODUCT_17_ID = "item_17";
	public const string PRODUCT_18_ID = "item_18";

    //public const string REFUNDED = "android.test.refunded";
    //public const string CANCELED = "android.test.canceled";
    //public const string PURCHASED = "android.test.purchased";

    /** LifeTimeVGs **/

    public static VirtualGood NO_ADS_LTVG = new LifetimeVG(
        "No Ads", // name
        "No Ads", // description
        NO_ADS_ID, // item id
        new PurchaseWithMarket(NO_ADS_ID, 0.99)); // the way this virtual good is purchased

    /** Virtual Goods **/

    public static VirtualGood PRODUCT_1 = new SingleUseVG(
        "Extra Moves 2", // name
        "Extra Moves 2", // description
        PRODUCT_1_ID, // item id
        new PurchaseWithMarket(PRODUCT_1_ID, 0.99)); // the way this virtual good is purchased

    public static VirtualGood PRODUCT_2 = new SingleUseVG(
        "Extra Moves 5", // name
		"Extra Moves 5", // description
        PRODUCT_2_ID, // item id
        new PurchaseWithMarket(PRODUCT_2_ID, 2.99)); // the way this virtual good is purchased

    public static VirtualGood PRODUCT_3 = new SingleUseVG(
		"Extra Moves 10", // name
		"Extra Moves 10", // description
        PRODUCT_3_ID, // item id
        new PurchaseWithMarket(PRODUCT_3_ID, 4.99)); // the way this virtual good is purchased

    public static VirtualGood PRODUCT_4 = new SingleUseVG(
        "Gold Package 50", // name
		"Gold Package 50", // description
        PRODUCT_4_ID, // item id
        new PurchaseWithMarket(PRODUCT_4_ID, 0.99)); // the way this virtual good is purchased

    public static VirtualGood PRODUCT_5 = new SingleUseVG(
        "Gold Package 200", // name
        "Gold Package 200", // description
        PRODUCT_5_ID, // item id
        new PurchaseWithMarket(PRODUCT_5_ID, 2.99)); // the way this virtual good is purchased
	
	public static VirtualGood PRODUCT_6 = new SingleUseVG(
		"Single Breaker", // name
		"Single Breaker", // description
		PRODUCT_6_ID, // item id
		new PurchaseWithMarket(PRODUCT_6_ID, 0.99)); // the way this virtual good is purchased

	public static VirtualGood PRODUCT_7 = new SingleUseVG(
		"Row Breaker", // name
		"Row Breaker", // description
		PRODUCT_7_ID, // item id
		new PurchaseWithMarket(PRODUCT_7_ID, 1.99)); // the way this virtual good is purchased

	public static VirtualGood PRODUCT_8 = new SingleUseVG(
		"Column Breaker", // name
		"Column Breaker", // description
		PRODUCT_8_ID, // item id
		new PurchaseWithMarket(PRODUCT_8_ID, 1.99)); // the way this virtual good is purchased

	public static VirtualGood PRODUCT_9 = new SingleUseVG(
		"Oven Breaker", // name
		"Oven Breaker", // description
		PRODUCT_9_ID, // item id
		new PurchaseWithMarket(PRODUCT_9_ID, 2.99)); // the way this virtual good is purchased

	public static VirtualGood PRODUCT_10 = new SingleUseVG(
		"Rainbow Breaker", // name
		"Rainbow Breaker", // description
		PRODUCT_10_ID, // item id
		new PurchaseWithMarket(PRODUCT_10_ID, 2.99)); // the way this virtual good is purchased
	
	public static VirtualGood PRODUCT_11 = new SingleUseVG(
		"Begin Rainbow", // name
		"Begin Rainbow", // description
		PRODUCT_11_ID, // item id
		new PurchaseWithMarket(PRODUCT_11_ID, 3.99)); // the way this virtual good is purchased

	public static VirtualGood PRODUCT_12 = new SingleUseVG(
		"Bomb Breaker", // name
		"Bomb Breaker", // description
		PRODUCT_12_ID, // item id
		new PurchaseWithMarket(PRODUCT_12_ID, 4.99)); // the way this virtual good is purchased

	public static VirtualGood PRODUCT_13 = new SingleUseVG(
		"500 Gold", // name
		"500 Gold", // description
		PRODUCT_13_ID, // item id
		new PurchaseWithMarket(PRODUCT_13_ID, 6.99)); // the way this virtual good is purchased

	public static VirtualGood PRODUCT_14 = new SingleUseVG(
		"1000 Gold", // name
		"1000 Gold", // description
		PRODUCT_14_ID, // item id
		new PurchaseWithMarket(PRODUCT_14_ID, 8.99)); // the way this virtual good is purchased

	public static VirtualGood PRODUCT_15 = new SingleUseVG(
		"5000 Gold", // name
		"5000 Gold", // description
		PRODUCT_15_ID, // item id
		new PurchaseWithMarket(PRODUCT_15_ID, 19.99)); // the way this virtual good is purchased

	public static VirtualGood PRODUCT_16 = new SingleUseVG(
		"10000 Gold", // name
		"10000 Gold", // description
		PRODUCT_16_ID, // item id
		new PurchaseWithMarket(PRODUCT_16_ID, 29.99)); // the way this virtual good is purchased

	public static VirtualGood PRODUCT_17 = new SingleUseVG(
		"20000 Gold", // name
		"20000 Gold", // description
		PRODUCT_17_ID, // item id
		new PurchaseWithMarket(PRODUCT_17_ID, 39.99)); // the way this virtual good is purchased

	public static VirtualGood PRODUCT_18 = new SingleUseVG(
		"50000 Gold", // name
		"50000 Gold", // description
		PRODUCT_18_ID, // item id
		new PurchaseWithMarket(PRODUCT_18_ID, 69.99)); // the way this virtual good is purchased
}