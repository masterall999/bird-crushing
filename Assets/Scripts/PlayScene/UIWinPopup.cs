﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIWinPopup : MonoBehaviour 
{
    public Text levelText;
    public Text scoreText;
    public Text bonusText;
    public Image cake;
    public Image star1;
    public Image star2;
    public Image star3;
    public Text buttonText;

	void Start () 
    {
        var board = GameObject.Find("Board").GetComponent<Board>();

        var star = board.star;

        levelText.text = "Level " + LevelLoader.instance.level.ToString();

        switch (star)
        {
            case 1:
                star1.gameObject.SetActive(true);
                star2.gameObject.SetActive(false);
                star3.gameObject.SetActive(false);
                break;
            case 2:
                star1.gameObject.SetActive(true);
                star2.gameObject.SetActive(true);
                star3.gameObject.SetActive(false);
                break;
            case 3:
                star1.gameObject.SetActive(true);
                star2.gameObject.SetActive(true);
                star3.gameObject.SetActive(true);
                break;
            default:
                star1.gameObject.SetActive(false);
                star2.gameObject.SetActive(false);
                star3.gameObject.SetActive(false);
                break;
        }

        if (star == 1)
        {
            bonusText.text = Configure.instance.bonus1Star.ToString();
        }
        else if (star == 2)
        {
            bonusText.text = Configure.instance.bonus2Star.ToString();
        }
        else if (star == 3)
        {
            bonusText.text = Configure.instance.bonus3Star.ToString();
        }
        else
        {
            bonusText.text = "0";
        }

        scoreText.text = "Score: " + board.score.ToString();

        var name = "cake_" + LevelLoader.instance.cake + "_4";
        //cake.sprite = Resources.Load<Sprite>(Configure.Cake(name));

        // the button text change from next to close when the last level
        if (LevelLoader.instance.level == Configure.instance.maxLevel)
        {
            buttonText.text = "Close";
        }
	}

    public void MapAutoPopup()
    {
        Configure.instance.autoPopup = LevelLoader.instance.level + 1;
    }
}
