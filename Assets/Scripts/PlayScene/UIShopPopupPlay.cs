﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIShopPopupPlay : MonoBehaviour 
{
    public Text coinAmount;

	void Start () 
    {
        UpdateCoinAmountLabel();
	}

    public void ButtonClickAudio()
    {
        AudioManager.instance.ButtonClickAudio();
    }

    public void UpdateCoinAmountLabel()
    {
        coinAmount.text = GameData.instance.GetPlayerCoin().ToString();
    }
}
