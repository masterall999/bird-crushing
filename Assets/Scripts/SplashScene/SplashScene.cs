﻿using UnityEngine;
using System.Collections;

// Soomla store
#if UNITY_ANDROID || UNITY_IOS
using Soomla;
using Soomla.Store;
#endif

public class SplashScene : MonoBehaviour 
{
    void Start()
    {
        // Request Interstitial Google Ads
        GoogleAdsController.instance.RequestInterstitial();
		GoogleAdsController.instance.RequestBanner ();

        // Soomla Store
        StoreEventHandler.instance.InitializeEventHandler();
        SoomlaStore.Initialize(new StoreAsset());
    }

    public void TouchStartClick()
    {
        GetComponent<SceneTransition>().PerformTransition();
    }
}