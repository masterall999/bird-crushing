﻿using UnityEngine;
using System.Collections;

public class ShopButton : MonoBehaviour {
	public PopupOpener shopPopup;

	public void OnClick() {
		AudioManager.instance.ButtonClickAudio();

		if (!GameObject.Find("ShopPopupMap(Clone)"))
			shopPopup.OpenPopup();
	}
}